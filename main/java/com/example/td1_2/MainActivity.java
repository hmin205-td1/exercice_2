package com.example.td1_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_file_name);
        LinearLayout layout = (LinearLayout)findViewById(R.id.info);

        TextView tv =(TextView)findViewById(R.id.text_view_id);
        EditText mEdit=(EditText)findViewById(R.id.editText1);
        Button button = (Button) findViewById(R.id.mainbutton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                tv.setText(mEdit.getText().toString());
            }
        });

    }
}